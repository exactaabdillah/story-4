from django.shortcuts import render
from django.http import HttpResponse

def index(request):
	return render (request, "STORY3.html")
	
def about(request):
	return render (request, "STORY3_1.html")

def website(request):
	return render (request, "STORY3_2.html")
	
def project(request):
	return render (request, "STORY3_3.html")
	
def navbar(request):
	return render (request, "navbar.html")


# Create your views here.
